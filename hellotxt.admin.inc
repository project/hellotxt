<?php

/**
 * @file
 * Administrative page callbacks for the hellotxt module.
 */

/**
 * Administration settings form.
 *
 * @see system_settings_form()
 */
function hellotxt_admin_settings() {
  $form = array();

  $form['account'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Authentication'),
    '#description' => t('A HelloTxt User Key to use when contents are posted.'),
  );
  $form['account']['hellotxt_user_key'] = array(
    '#type'          => 'textfield',
    '#title'         => t('User key'),
    '#default_value' => variable_get('hellotxt_user_key', NULL),
    '#description'   => t('Get a <a href="@user-key">User Key</a>.', array('@user-key' => url('http://hellotxt.com/settings/api/hellotxt-Drupal-module'))),
  );

  $form['posting'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Posting'),
    '#description' => t('Allowed users will be given the option to post to HelloTxt accounts when they create new content.'),
  );
  $form['posting']['hellotxt_types'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Node types'),
    '#options'       => node_get_types('names'),
    '#default_value' => variable_get('hellotxt_types', array('blog' => 'blog')),
  );
  $form['posting']['hellotxt_default_format'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Default format string'),
    //'#maxlength'     => 140,
    '#description'   => t('The given text will be posted to HelloTxt. You can use !url, !title and !user as replacement text.'),
    '#default_value' => variable_get('hellotxt_default_format', 'New post: !title !url'),
  );
  $form['posting']['hellotxt_group'] = array(
    '#type'          => 'select',
    '#title'         => t('Default group'),
    '#options'       => array(
      'inhome'    => t('inhome'),
      'friend'    => t('friend'),
      'colleague' => t('colleague'),
    ),
    '#default_value' => variable_get('hellotxt_default_group', 'inhome'),
  );

  $form['debug'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Debug'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );
  $form['debug']['hellotxt_debug'] = array(
    '#type'          => 'radios',
    '#title'         => t('Debug mode'),
    '#options'       => array(
      '0' => '0',
      '1' => '1',
    ),
    '#default_value' => variable_get('hellotxt_debug', '0'),
  );

  return system_settings_form($form);
}