<?php

/**
 * Post a message to a HelloTxt.
 *
 * @param $user_key string
 *   User API key.
 * @param $body string
 *   New status text.
 * @param $title string
 *   Title of the posted message. This will only appear if the specified service supports a title field.
 * @param $group string
 *   The Group of serives to update. (inhome, friend, colleague).
 * @param $debug string
 *   Set this value to "1" to avoid posting test data.
 * @return
 *   XML response from HelloTxt API or string cURL error message.
 */
function hellotxt_user_post($user_key, $body, $title = '', $group = 'inhome', $debug = '1') {
  $api_url  = API_URL . 'method/';
  $method   = 'user.post';
  $app_key  = API_KEY;
  $user_key = variable_get('hellotxt_user_key', NULL);
  $request_url = $api_url . $method;
  $session = curl_init();
  curl_setopt($session, CURLOPT_URL, $request_url);
  curl_setopt($session, CURLOPT_POST, TRUE);
  curl_setopt($session, CURLOPT_POSTFIELDS, 
    'app_key='   . $app_key .
    '&user_key=' . $user_key .
    '&body='     . $body .
    '&title='    . $title .
    '&group='    . $group .
    '&debug='    . $debug .
    '&');
  curl_setopt($session, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($session, CURLOPT_TIMEOUT, 3);
  curl_setopt($session, CURLOPT_FAILONERROR, TRUE);
  $result = curl_exec($session);
  $error  = curl_error($session);
  curl_close($session);
  if (!empty($error)) {
    
    return $error;
  }
  else {
    $result = _hellotxt_result($result);
    
    return $result['message'];
  }
}

/**
 * Attempts to authenticate a User Key on HelloTxt.
 *
 * @param $user_key string
 *   User Key.
 * @return boolean
 */
function hellotxt_authenticate($user_key) {
  return;
}

/**
 * Internal XML converter
 *
 * @param $data string
 *   XML string
 */
function _hellotxt_result($data) {
  $result = array();
  
  $xml = new SimpleXMLElement($data);
  $status = $xml['status'];
  switch ($status) {
    case 'OK':
    case 'DEBUG':
      $result['message'] = $status;
      break;
    case 'FAIL':
      $result['message'] = $xml->message;
      break;
  }
  
  return $result;
}